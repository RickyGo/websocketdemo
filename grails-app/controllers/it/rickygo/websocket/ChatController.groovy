package it.rickygo.websocket

import grails.plugin.springsecurity.annotation.Secured
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.SendTo

import java.security.Principal

@Secured(['ROLE_ADMIN', 'ROLE_USER'])
class ChatController {

    def index() {}

    @MessageMapping("/message")
    @SendTo("/topic/chat")
    protected String hello(String msg, Principal principal) {
        return "${principal.name}: $msg"
    }

    @MessageMapping("/connect")
    @SendTo("/topic/chat")
    protected String connect(String msg, Principal principal) {
        return "${principal.name} connected"
    }

    @MessageMapping("/disconnect")
    @SendTo("/topic/chat")
    protected String disconnect(String msg, Principal principal) {
        return "${principal.name} disconnected"
    }

}
