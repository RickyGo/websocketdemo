<%--
  Created by IntelliJ IDEA.
  User: riccardo
  Date: 16/05/2015
  Time: 14:34
--%>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>

    <asset:javascript src="jquery"/>
    <asset:javascript src="spring-websocket"/>

    <script type="text/javascript">
        $(function () {
            var socket = new SockJS("${createLink(uri: '/stomp')}");
            var client = Stomp.over(socket);

            client.connect({}, function () {
                client.subscribe("/topic/chat", function (message) {
                    $("#chatLog").append(message.body + "</br>");
                });
                client.send("/app/connect", {}, "connect");
            });

            $('#disconnect').click(function () {
                client.send("/app/disconnect", {}, "disconnect");
                client.disconnect();
            });

            $("#sendButton").click(function () {
                var msgBox = $('#message');
                if(msgBox.val()) {
                    client.send("/app/message", {}, msgBox.val());
                    msgBox.val('');
                }
            });
        });
    </script>
    <style>
        #chatLog{
            overflow: auto;
            margin: 10px;
        }
        #disconnect{
            float: right;
        }
        #message{
            width: 250px;
            height: 30px;
        }
        div.container{
            margin: 10px;
        }
    </style>
</head>

<body>
<div class="container">
    <button id="disconnect">disconnect</button>
    <button id="sendButton">send</button>
    <textarea id="message" rows="2" cols="50"></textarea>
    <div id="chatLog"></div>
</div>
</body>
</html>