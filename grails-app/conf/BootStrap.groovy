import it.rickygo.websocket.Role
import it.rickygo.websocket.User
import it.rickygo.websocket.UserRole

class BootStrap {

    def init = { servletContext ->
        def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
        def userRole = new Role(authority: 'ROLE_USER').save(flush: true)

        def testUser = new User(username: 'me', password: 'password')
        def adminUser = new User(username: 'admin', password: 'password')
        testUser.save(flush: true)
        adminUser.save(flush: true)

        UserRole.create testUser, userRole, true
        UserRole.create adminUser, adminRole, true
        UserRole.create adminUser, userRole, true
    }
    def destroy = {
    }
}
